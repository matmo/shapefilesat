# shapefilesat

This is an R package which collects Austrian shapefiles for use with the [sf package](https://cran.r-project.org/web/packages/sf/index.html)

* Years covered: 2004, 2007, 2009, 2011-2019
* All shapefiles reprojected to EPSG 31287 / MGI Austria Lambert

This package is rather big.

## Installation

```R
install.packages("devtools")
devtools::install_gitlab("matmosr/shapefilesat")
```