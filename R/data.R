#' Shapefile for Austrian municipalities, 2019 geography
#'
#' A shapefile of class sf for all Austrian municipalities
#'
#' \itemize{
#'   \item id. GKZ of the Austrian municipality
#'   \item name. Name of municipality
#' }
#'
#' @docType data
#' @keywords datasets
#' @name shp2019
#' @usage data(shp2019)
#' @format Simple feature collection with 2118 features and 2 fields
#' @source https://data.statistik.gv.at/web/meta.jsp?dataset=OGDEXT_GEM_1
NULL

#' Shapefile for Austrian municipalities, 2018 geography
#'
#' A shapefile of class sf for all Austrian municipalities
#'
#' \itemize{
#'   \item id. GKZ of the Austrian municipality
#'   \item name. Name of municipality
#' }
#'
#' @docType data
#' @keywords datasets
#' @name shp2018
#' @usage data(shp2018)
#' @format Simple feature collection with 2120 features and 2 fields
#' @source https://data.statistik.gv.at/web/meta.jsp?dataset=OGDEXT_GEM_1
NULL

#' Shapefile for Austrian municipalities, 2017 geography
#'
#' A shapefile of class sf for all Austrian municipalities
#'
#' \itemize{
#'   \item id. GKZ of the Austrian municipality
#'   \item name. Name of municipality
#' }
#'
#' @docType data
#' @keywords datasets
#' @name shp2017
#' @usage data(shp2017)
#' @format Simple feature collection with 2122 features and 2 fields
#' @source https://data.statistik.gv.at/web/meta.jsp?dataset=OGDEXT_GEM_1
NULL

#' Shapefile for Austrian municipalities, 2016 geography
#'
#' A shapefile of class sf for all Austrian municipalities
#'
#' \itemize{
#'   \item id. GKZ of the Austrian municipality
#'   \item name. Name of municipality
#' }
#'
#' @docType data
#' @keywords datasets
#' @name shp2016
#' @usage data(shp2016)
#' @format Simple feature collection with 2122 features and 2 fields
#' @source https://data.statistik.gv.at/web/meta.jsp?dataset=OGDEXT_GEM_1
NULL

#' Shapefile for Austrian municipalities, 2015 geography
#'
#' A shapefile of class sf for all Austrian municipalities
#'
#' \itemize{
#'   \item id. GKZ of the Austrian municipality
#'   \item name. Name of municipality
#' }
#'
#' @docType data
#' @keywords datasets
#' @name shp2015
#' @usage data(shp2015)
#' @format Simple feature collection with 2124 features and 2 fields
#' @source https://data.statistik.gv.at/web/meta.jsp?dataset=OGDEXT_GEM_1
NULL

#' Shapefile for Austrian municipalities, 2014 geography
#'
#' A shapefile of class sf for all Austrian municipalities
#'
#' \itemize{
#'   \item id. GKZ of the Austrian municipality
#'   \item name. Name of municipality
#' }
#'
#' @docType data
#' @keywords datasets
#' @name shp2014
#' @usage data(shp2014)
#' @format Simple feature collection with 2376 features and 2 fields
#' @source https://data.statistik.gv.at/web/meta.jsp?dataset=OGDEXT_GEM_1
NULL

#' Shapefile for Austrian municipalities, 2013 geography
#'
#' A shapefile of class sf for all Austrian municipalities
#'
#' \itemize{
#'   \item id. GKZ of the Austrian municipality
#'   \item name. Name of municipality
#' }
#'
#' @docType data
#' @keywords datasets
#' @name shp2013
#' @usage data(shp2013)
#' @format Simple feature collection with 2376 features and 2 fields
#' @source https://data.statistik.gv.at/web/meta.jsp?dataset=OGDEXT_GEM_1
NULL

#' Shapefile for Austrian municipalities, 2012 geography
#'
#' A shapefile of class sf for all Austrian municipalities
#'
#' \itemize{
#'   \item id. GKZ of the Austrian municipality
#'   \item name. Name of municipality
#' }
#'
#' @docType data
#' @keywords datasets
#' @name shp2012
#' @usage data(shp2012)
#' @format Simple feature collection with 2379 features and 2 fields
#' @source https://data.statistik.gv.at/web/meta.jsp?dataset=OGDEXT_GEM_1
NULL

#' Shapefile for Austrian municipalities, 2011 geography
#'
#' A shapefile of class sf for all Austrian municipalities
#'
#' \itemize{
#'   \item id. GKZ of the Austrian municipality
#'   \item name. Name of municipality
#' }
#'
#' @docType data
#' @keywords datasets
#' @name shp2011
#' @usage data(shp2011)
#' @format Simple feature collection with 2379 features and 2 fields
#' @source https://data.statistik.gv.at/web/meta.jsp?dataset=OGDEXT_GEM_1
NULL

#' Shapefile for Austrian municipalities, 2009 geography
#'
#' A shapefile of class sf for all Austrian municipalities
#'
#' \itemize{
#'   \item id. GKZ of the Austrian municipality
#'   \item name. Name of municipality
#' }
#'
#' @docType data
#' @keywords datasets
#' @name shp2009
#' @usage data(shp2009)
#' @format Simple feature collection with 2379 features and 2 fields
#' @source http://www.bev.gv.at
NULL

#' Shapefile for Austrian municipalities, 2007 geography
#'
#' A shapefile of class sf for all Austrian municipalities
#'
#' \itemize{
#'   \item id. GKZ of the Austrian municipality
#'   \item name. Name of municipality
#' }
#'
#' @docType data
#' @keywords datasets
#' @name shp2007
#' @usage data(shp2007)
#' @format Simple feature collection with 2379 features and 2 fields
#' @source http://www.bev.gv.at
NULL

#' Shapefile for Austrian municipalities, 2004 geography
#'
#' A shapefile of class sf for all Austrian municipalities
#'
#' \itemize{
#'   \item id. GKZ of the Austrian municipality
#'   \item name. Name of municipality
#' }
#'
#' @docType data
#' @keywords datasets
#' @name shp2004
#' @usage data(shp2004)
#' @format Simple feature collection with 2381 features and 2 fields
#' @source http://www.bev.gv.at
NULL

